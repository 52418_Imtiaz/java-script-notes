// varibale declatation, if-else, loop, arrays, functions, print etc,

//Memory Phase => reserve space for variables (not value) and stores functions

//Code Phase => assign value to respective variables and execute functions/statement (it will ignore function declaration)

// whenever a function is executed(is called), a new execution context is created

//when function ends or return, its execution context gets destroyed

// lexical env => local env + lexical env of parent

// == : check for value, === : check for value and data type (no implicit coercion happens)

// Object => key: value pair

// Shallow copy => point to same object

// Deep copy =>  create a new copy of object in memory

// Hoisting => Property in jS through which we can use use varibale or function before it is declared.

// var x = 5

// square(x)

// function square(num) {
//   var result = num * num
//   console.log(result)
// }

 console.log("5" + (6 + 5)) //"56"+5 => 565

// console.log(5 + 6 + "5") //11+"5" => 115

// console.log(5 + "6" + 7 + 1)

// coerision

// var x = 5

// if (x === "5") {
//   console.log("it is true")
// } else {
//   console.log("it is false")
// }

// var x = 9

// function test() {
//   var y = 99
//   console.log(x)
// }

// console.log(y)
// test()

// var x = 7 // Number
// var city = "delhi" //String
// var isAvailable = 5 == 6 // boolean

// var list = ["shubham", "aditya", "rohit"]

// var user = {
//   name: "Shubham",
//   age: 40,
//   address: {
//     city: "Delhi",
//     state: "Delhi",
//     pincode: "923892",
//   },
//   contact: "9876543210",
// }

// user.name = "Udbhav"
// user.age = 25

// console.log(user)

// var user1 = {
//   name: "Shubham",
//   age: 40,
//   address: {
//     city: "Delhi",
//     state: "Delhi",
//     pincode: "923892",
//   },
//   contact: "9876543210",
// }

// var user2 = user1 //shallow copy

// user2.name = "udbhav"

// console.log(user1.name)

// var user1 = {
//   name: "Shubham",
//   age: 40,
//   contact: "9876543210",
// }

// // var user2 = Object.assign({}, user1) //deep copy
// var user2 = {...user1} // deep copy

// user1.name = "udbhav"

// console.log(user2)

// var x = 23

// console.log(x > 20 ? "gt 20" : "it is false")

// var y = x > 20 ? 100 : 10

// console.log(y)

// console.log(x)

// sayHello()

// var x = 90

// function sayHello() {
//   console.log("Hey everyone")
// }
